# json-lookup

Json lookup is a library in Go that performs search and retrieve operations in a JSON of your choice. It attacks a very specific need: avoiding mapping of large objects to DTOs where they aren't needed. 

- If you've worked with Go code proffessionally, you may have faced situations where you need one or two fields from a large and extensive JSon, that may have nested values. You probably would have to create a big DTO to fetch them. Well, Json-lookup is made to avoid creating those big DTOs that are left mostly commented or without 90% of their attributes put to use. 

## Installation

As with any library, you may do:

```bash
go get gitlab.com/a.vandam/json-lookup
```

## Usage

This section will be improved the following weeks. It's use, however, is exemplified here:

```golang
    valueFetched, err := json-lookup.String(body, "key1.key2.key3.key4")
```
The following weeks we'll be adding integration tests that simulate real life uses. 


## Contributing
Pull requests are welcome, of course. As this is a side project, we will try to expand it but it depends on my free time.

Please make sure to update tests as appropriate.


