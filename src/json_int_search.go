package lookup

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

/*IntDefaultReturnValue is returned when error happens.*/
const IntDefaultReturnValue int = -9999

/*Int allows to lookup an Int value in a json. If an error is found, IntDefaultReturnValue is returned*/
func Int(contents *[]byte, path string) (int, error) {
	keys := strings.Split(path, ".")

	if len(keys) == 0 {
		return IntDefaultReturnValue, fmt.Errorf("no key to look for: %v", path)
	}
	tempStorageMap := make(map[string]interface{})
	//fmt.Printf("\n debug: %+v", string(*contents))
	err := json.Unmarshal(*contents, &tempStorageMap)
	//fmt.Printf("\n unmarshaller debug: %+v", tempStorageMap)
	if err != nil {
		return IntDefaultReturnValue, fmt.Errorf("found error while unmarshalling: %v", err)
	}
	//setting up key to lookup for
	keyToLookup := keys[0]
	remainingHierarchyCalls := len(keys) - 1
	//found value?
	valueFound, valuePresent := tempStorageMap[keyToLookup]
	if !valuePresent {
		return IntDefaultReturnValue, fmt.Errorf("key (%v) not found with a value", keyToLookup)
	}

	value, parseableToInt := parseToInt(valueFound)
	//fmt.Printf("\n @@@@: %v, %v %v", valueFound, value, parseableToInt)

	//fmt.Printf("\n string value %v, stringable:  %v, keys: %v, searching now: %v", value, parseableToInt, keys, keyToLookup)
	if !parseableToInt && remainingHierarchyCalls <= 0 {
		//case: dont attempt to parse again, it's the final hierarchy
		return IntDefaultReturnValue, fmt.Errorf("value of key (%v) not parseable to int", keyToLookup)
	}
	//try again
	if !parseableToInt && remainingHierarchyCalls > 0 {
		newPtr, invalidNextLevel := json.Marshal(valueFound)
		if invalidNextLevel != nil {
			return IntDefaultReturnValue, fmt.Errorf("found error while unmarshalling key: %v, error: %v", valueFound, err)
		}
		restOfPath := strings.Join(keys[1:], ".")
		fmt.Printf("\n keys: %v,restOfP: %v", keys[1:], restOfPath)
		value, err := Int(&newPtr, restOfPath)
		fmt.Printf("\n recursive returned: %v,err: %v", value, err)
		return value, err
	}

	return value, nil
}

func parseToInt(valueFound interface{}) (valueAsInt int, parseable bool) {
	valueAsFloat, parseable := valueFound.(float64)
	if !parseable {
		valueAsInt, parseable = parseToIntUsingStrings(valueFound)
		return valueAsInt, parseable
	}
	//deferring this function allows to recover from panic of an invalid parsing to int
	defer func() {
		parseError := recover()
		if parseError != nil {
			//overrides return value
			valueAsInt = IntDefaultReturnValue
			parseable = false
		}
	}()

	valueAsInt = int(valueAsFloat)

	return valueAsInt, parseable

}
func parseToIntUsingStrings(valueFound interface{}) (valueAsInt int, parseable bool) {
	defer func() {
		parseError := recover()
		if parseError != nil {
			//overrides return value
			valueAsInt = IntDefaultReturnValue
			parseable = false
		}
	}()

	valueAsInt, err := strconv.Atoi(valueFound.(string))
	if err != nil {
		return IntDefaultReturnValue, false
	}
	return valueAsInt, true
}
