package lookup

import (
	"fmt"
	"os"
	"testing"

	assert "github.com/stretchr/testify/assert"
)

func TestJsonConfigurableStringLookup(t *testing.T) {
	configurations := StringLookupConfigs{
		DefaultReturnString: "Blablablabla!",
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	testCases := []jsonConfigurableStringLookupTestCase{
		{
			testName:       "testing first level lookup",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     configurations,
			expectedReturn: "first found",
			expectedErr:    false,
		},
		{
			testName:       "testing first level insensitive lookup",
			fileName:       "configured_example_string.json",
			keyToLookup:    "fIrStLeVeL",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NoValueFoundErrorMsg("fIrStLeVeL").Error(),
		},
		{
			testName:       "testing first level not found",
			fileName:       "configured_example_string.json",
			keyToLookup:    "missingFirstLevel",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NoValueFoundErrorMsg("missingFirstLevel").Error(),
		},
		{
			testName:       "testing first level found but not stringable",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NotStringableErrorMsg("firstLevelObject", "{}").Error(),
		},
		/*Second level key examples.*/
		{
			testName:       "testing second level value found ",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevel",
			msgConfigs:     configurations,
			expectedReturn: "one down",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing second level value not found ",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.inexistentKey",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NoValueFoundErrorMsg("inexistentKey").Error(),
		},
		{
			testName:       "testing second level value not stringable ",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NotStringableErrorMsg("secondLevelObject", "no value found").Error(),
		},
		{
			testName:       "testing second level value is an int, not a string ",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelInt",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NotStringableErrorMsg("secondLevelInt", "").Error(),
		},
		{
			testName:       "testing second level value is empty object",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelEmptyObject",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NotStringableErrorMsg("firstLevelObject.secondLevelEmptyObject", "").Error(),
		},
		{
			testName:       "testing third level value lookup",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelKey",
			msgConfigs:     configurations,
			expectedReturn: "mayday!",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing third level value is empty object",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelEmptyObject",
			msgConfigs:     configurations,
			expectedReturn: configurations.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: configurations.NotStringableErrorMsg("thirdLevelEmptyObject", "{}").Error(),
		},
		{
			testName:       "testing third and fourth level value lookup",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelObject.fourthLevelObject.fifthLevelObject.nestedKey",
			msgConfigs:     configurations,
			expectedReturn: "finally!",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		// Tests for validating erroneus configurations

	}

	for i, jsonTCase := range testCases {
		t.Logf("testCase number %v of json string lookup", i)

		jsonTCase.Run(t)
	}

}

// Tests for validating erroneus configurations

func TestErroneusConfigurations(t *testing.T) {
	fullConfigOk := StringLookupConfigs{
		DefaultReturnString: "OK!",
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	missingDefaultReturnString := StringLookupConfigs{
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},

		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	missingEmptyKeyErrorMsg := StringLookupConfigs{
		DefaultReturnString: "Blah!",

		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	missingNotStringable := StringLookupConfigs{
		DefaultReturnString: "Blah!",

		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},

		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	missingUnmarshallErrorMsg := StringLookupConfigs{

		DefaultReturnString: "Blah!",
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	missingNextLevelUnmarshallErrorMsg := StringLookupConfigs{

		DefaultReturnString: "Blah!",
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NoValueFoundErrorMsg: func(keyToLookup string) error {
			return fmt.Errorf("i failed, sempai: %v", keyToLookup)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
	}
	missingNoValueFoundMsg := StringLookupConfigs{
		DefaultReturnString: "Blah!",
		EmptyKeyErrorMsg: func(keysSent []string) error {
			return fmt.Errorf("keys sent!?!?!?: %v", keysSent)
		},
		NotStringableErrorMsg: func(keyToLookup string, valueFound interface{}) error {
			return fmt.Errorf("it's not parseable. please, stop")
		},
		UnmarshallingErrorMsg: func(keysSent []string, unmErr error) error {
			return fmt.Errorf("the key to your heart, was unmarshallable: %v", keysSent)
		},
		NextLevelUnmarshErrorMsg: func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error {
			return fmt.Errorf("nextLevelUnmarshalling faced an error: %v", keyToLookup)
		},
	}
	testCases := []jsonConfigurableStringLookupTestCase{
		{
			testName:       "testing first level lookup with all configs OK!",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     fullConfigOk,
			expectedReturn: "first found",
			expectedErr:    false,
		},
		{
			testName:       "testing first level lookup with default return not set",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevelInexistent",
			msgConfigs:     missingDefaultReturnString,
			expectedReturn: missingDefaultReturnString.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: missingDefaultReturnString.NoValueFoundErrorMsg("firstLevelInexistent").Error(),
		},

		{
			testName:       "forgot to set the error message function for a key not sent",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     missingEmptyKeyErrorMsg,
			expectedReturn: missingEmptyKeyErrorMsg.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: fmt.Sprintf(StringSearchMissingFunctionsErrorMsg, NoKeySentErrorFunctionDesc),
		},

		{
			testName:       "forgot to set the error message function for a problem while unmarshalling next level",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     missingNextLevelUnmarshallErrorMsg,
			expectedReturn: missingNextLevelUnmarshallErrorMsg.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: fmt.Sprintf(StringSearchMissingFunctionsErrorMsg, NextLevelUnmarshallErrorFunctionDesc),
		},

		{
			testName:       "forgot to set a value not found error message function",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     missingNoValueFoundMsg,
			expectedReturn: missingNoValueFoundMsg.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: fmt.Sprintf(StringSearchMissingFunctionsErrorMsg, ValueNotFoundErrorFunctionDesc),
		},

		{
			testName:       "forgot to set an impossible to convert to a string error message function",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     missingNotStringable,
			expectedReturn: missingNotStringable.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: fmt.Sprintf(StringSearchMissingFunctionsErrorMsg, UnableToParseToStringErrorFunctionDesc),
		},
		{
			testName:       "forgot to set an impossible to unmarshall the received json error function",
			fileName:       "configured_example_string.json",
			keyToLookup:    "firstLevel",
			msgConfigs:     missingUnmarshallErrorMsg,
			expectedReturn: missingUnmarshallErrorMsg.DefaultReturnString,
			expectedErr:    true,
			expectedErrMsg: fmt.Sprintf(StringSearchMissingFunctionsErrorMsg, UnmarshallingErrorFunctionDesc),
		},
	}

	for i, jsonTCase := range testCases {
		t.Logf("testCase number %v of json string lookup", i)

		jsonTCase.Run(t)
	}

}

type jsonConfigurableStringLookupTestCase struct {
	testName    string
	fileName    string
	keyToLookup string
	msgConfigs  StringLookupConfigs
	//replace this return with each expected type
	expectedReturn string
	expectedErr    bool
	expectedErrMsg string
}

func (testCase *jsonConfigurableStringLookupTestCase) Run(t *testing.T) {
	t.Logf("--------------------------------------------------")
	t.Logf("running test: %v", testCase.testName)
	t.Logf("reading file: %v", testCase.fileName)

	fileContent, err := os.ReadFile(testCase.fileName)
	if err != nil {
		t.Logf("error while reading file of name: %v", testCase.fileName)
		t.Logf("error reported: %v", err)
		t.Fatalf("error in test of name: %v while reading file: %v", testCase.testName, err)
	}

	//Function under test. Please change accordingly.
	got, err := StringWithConfig(&fileContent, testCase.keyToLookup, &testCase.msgConfigs)
	/*---------------------------------------------------*/
	//check test ok

	assertOk := assert.Equal(t, testCase.expectedReturn, got, "asserting got: %v and expected: %v", got, testCase.expectedReturn)

	if !assertOk {
		t.Logf("got vs expected is different, assert failed")
		t.Logf("function reported error? : err : %v", err)
		t.FailNow()
		return
	}
	t.Logf("got vs expected ok")
	//check if expected error and got error are the same
	if testCase.expectedErr {
		assertErrOk := assert.EqualError(t, err, testCase.expectedErrMsg, "asserting error got: %v and expected: %v", err, testCase.expectedErrMsg)
		if !assertErrOk {
			t.Logf("found an diff in expected error, assert failed: gotErr: %v, expectedErr: %v", err, testCase.expectedErrMsg)
			t.FailNow()
			return
		}
	} else {
		//managed like this as error can be nil, but errMsg no.
		if err != nil {
			t.Logf("found an unexpected error, gotErr: %v", err)
			t.FailNow()
			return
		}
	}
	t.Logf("test: %v: OK ", testCase.testName)
	//t.Logf("unhandled test case ??? testCase data: %+v VS test results: got :%v, err: %v", testCase, got, err)
	//t.FailNow()
}
