package lookup

import (
	"encoding/json"
	"fmt"
	"strings"
)

/*StringDefaultReturn is the value returned in case of errors.*/
const StringDefaultReturn string = ""

/*String allows you to lookup an string in a json path. If it falls, it will return "" and an error msg*/
func String(contents *[]byte, path string) (string, error) {
	keys := strings.Split(path, ".")

	if len(keys) == 0 {
		return StringDefaultReturn, fmt.Errorf("no key to look for: %v", path)
	}
	tempStorageMap := make(map[string]interface{})
	//fmt.Printf("\n debug: %+v", string(*contents))
	err := json.Unmarshal(*contents, &tempStorageMap)
	//fmt.Printf("\n unmarshaller debug: %+v", tempStorageMap)
	if err != nil {
		return StringDefaultReturn, fmt.Errorf("found error while unmarshalling: %v", err)
	}
	//setting up key to lookup for
	keyToLookup := keys[0]
	remainingHierarchyCalls := len(keys) - 1
	//found value?
	valueFound, valuePresent := tempStorageMap[keyToLookup]

	if !valuePresent {
		return StringDefaultReturn, fmt.Errorf("key (%v) not found with a value", keyToLookup)
	}
	//avoid a nil ptr exception
	if valueFound == nil {
		return StringDefaultReturn, fmt.Errorf("key (%v) not stringable", keyToLookup)
	}
	//parsing to string
	stringValue, stringable := valueFound.(string)
	//fmt.Printf("\n string value %v, stringable:  %v, keys: %v, searching now: %v", stringValue, stringable, keys, keyToLookup)
	if !stringable && remainingHierarchyCalls <= 0 {
		//case: dont attempt to parse again, it's the final hierarchy
		return StringDefaultReturn, fmt.Errorf("key (%v) not stringable", keyToLookup)
	}
	//try again
	if !stringable && remainingHierarchyCalls > 0 {
		newPtr, invalidNextLevel := json.Marshal(valueFound)
		if invalidNextLevel != nil {
			return StringDefaultReturn, fmt.Errorf("found error while unmarshalling key: %v, error: %v", valueFound, err)
		}
		restOfPath := strings.Join(keys[1:], ".")
		//fmt.Printf("\n keys: %v,restOfP: %v", keys[1:], restOfPath)
		stringValue, err := String(&newPtr, restOfPath)
		//fmt.Printf("\n recursive returned: %v,err: %v", stringValue, err)
		return stringValue, err
	}

	return stringValue, nil

}
