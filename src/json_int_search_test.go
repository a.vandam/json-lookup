//go:build int_test

package lookup

import (
	"os"
	"testing"

	assert "github.com/stretchr/testify/assert"
)

func TestJsonIntLookup(t *testing.T) {

	testCases := []intLookupTestCase{
		{
			testName:       "testing first level lookup",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelInteger",
			expectedReturn: 123,
			expectedErr:    false,
		},
		{
			testName:       "testing first level insensitive lookup",
			fileName:       "example_int.json",
			keyToLookup:    "fIrStLeVeLInteger",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "key (fIrStLeVeLInteger) not found with a value",
		},
		{
			testName:       "testing first level not found",
			fileName:       "example_int.json",
			keyToLookup:    "missingFirstLevel",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "key (missingFirstLevel) not found with a value",
		},
		{
			testName:       "testing first level found but not parseable to int",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (firstLevelObject) not parseable to int",
		},
		/*Second level key examples.*/
		{
			testName:       "testing second level value found ",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelInteger",
			expectedReturn: 100,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing second level value not found ",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.inexistentKey",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "key (inexistentKey) not found with a value",
		},
		{
			testName:       "testing second level value not parseable to int ",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (secondLevelObject) not parseable to int",
		},
		{
			testName:       "testing second level value is an int, not a string ",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelString",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (secondLevelString) not parseable to int",
		},
		{
			testName:       "testing second level value is empty object",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelEmptyObject",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (secondLevelEmptyObject) not parseable to int",
		},
		{
			testName:       "testing third level value lookup",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelKey",
			expectedReturn: 9999,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing third level value is empty object",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelEmptyObject",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (thirdLevelEmptyObject) not parseable to int",
		},
		{
			testName:       "testing third level value is an string",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelString",
			expectedReturn: IntDefaultReturnValue,
			expectedErr:    true,
			expectedErrMsg: "value of key (thirdLevelString) not parseable to int",
		},
		{
			testName:       "testing third level value is a parseable string",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.parseableString",
			expectedReturn: 29,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing third and fourth level value lookup",
			fileName:       "example_int.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelObject.fourthLevelObject.fifthLevelObject.nestedKey",
			expectedReturn: -23,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for i, jsonTCase := range testCases {
		t.Logf("testCase number %v of json string lookup", i)

		jsonTCase.Run(t)
	}

}

type intLookupTestCase struct {
	testName    string
	fileName    string
	keyToLookup string
	//TODO: replace this expected with the argument return type
	expectedReturn int
	expectedErr    bool
	expectedErrMsg string
}

func (testCase *intLookupTestCase) Run(t *testing.T) {
	t.Logf("running test: %v", testCase.testName)
	t.Logf("reading file: %v", testCase.fileName)

	fileContent, err := os.ReadFile(testCase.fileName)
	if err != nil {
		t.Logf("error while reading file of name: %v", testCase.fileName)
		t.Logf("error reported: %v", err)
		t.Fatalf("error in test %v while reading file: %v", testCase.testName, err)
	}

	/*	- - - - - - - - - - - -  Function under test. Please change accordingly.    - - - - -  - - - - - */
	got, err := LookupInt(&fileContent, testCase.keyToLookup)
	//check test ok

	assertOk := assert.Equal(t, testCase.expectedReturn, got, "asserting got: %v and expected: %v", got, testCase.expectedReturn)

	if !assertOk {
		t.Logf("got vs expected is different, assert failed")
		t.Logf("function reported error? : err : %v", err)
		t.FailNow()
		return
	}
	t.Logf("got vs expected ok")
	//check if expected error and got error are the same
	if testCase.expectedErr {
		assertErrOk := assert.EqualError(t, err, testCase.expectedErrMsg, "asserting error got: %v and expected: %v", err, testCase.expectedErrMsg)
		if !assertErrOk {
			t.Logf("found an diff in expected error, assert failed: gotErr: %v, expectedErr: %v", err, testCase.expectedErrMsg)
			t.FailNow()
			return
		}
	} else {
		//managed like this as error can be nil, but errMsg no.
		if err != nil {
			t.Logf("found an unexpected error, gotErr: %v", err)
			t.FailNow()
			return
		}
	}
	t.Logf("test: %v: OK ", testCase.testName)
	//t.Logf("unhandled test case ??? testCase data: %+v VS test results: got :%v, err: %v", testCase, got, err)
	//t.FailNow()
}
