package lookup

import (
	"os"
	"testing"

	assert "github.com/stretchr/testify/assert"
)

func TestJsonStringLookup(t *testing.T) {

	testCases := []jsonStringLookupTestCase{
		{
			testName:       "testing first level lookup",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevel",
			expectedReturn: "bingo! first level found",
			expectedErr:    false,
		},
		{
			testName:       "testing first level insensitive lookup",
			fileName:       "example_string.json",
			keyToLookup:    "fIrStLeVeL",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (fIrStLeVeL) not found with a value",
		},
		{
			testName:       "testing first level not found",
			fileName:       "example_string.json",
			keyToLookup:    "missingFirstLevel",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (missingFirstLevel) not found with a value",
		},
		{
			testName:       "testing first level found but not stringable",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (firstLevelObject) not stringable",
		},
		/*Second level key examples.*/
		{
			testName:       "testing second level value found ",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevel",
			expectedReturn: "bingo",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing second level value not found ",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.inexistentKey",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (inexistentKey) not found with a value",
		},
		{
			testName:       "testing second level value not stringable ",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (secondLevelObject) not stringable",
		},
		{
			testName:       "testing second level value is an int, not a string ",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelInt",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (secondLevelInt) not stringable",
		},
		{
			testName:       "testing second level value is empty object",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelEmptyObject",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (secondLevelEmptyObject) not stringable",
		},
		{
			testName:       "testing third level value lookup",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelKey",
			expectedReturn: "hey!",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			testName:       "testing third level value is empty object",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelEmptyObject",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (thirdLevelEmptyObject) not stringable",
		},
		{
			testName:       "testing third level value is an int",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelEmptyObject",
			expectedReturn: StringDefaultReturn,
			expectedErr:    true,
			expectedErrMsg: "key (thirdLevelEmptyObject) not stringable",
		},
		{
			testName:       "testing third and fourth level value lookup",
			fileName:       "example_string.json",
			keyToLookup:    "firstLevelObject.secondLevelObject.thirdLevelObject.fourthLevelObject.fifthLevelObject.nestedKey",
			expectedReturn: "awesome!",
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for i, jsonTCase := range testCases {
		t.Logf("testCase number %v of json string lookup", i)

		jsonTCase.Run(t)
	}

}

type jsonStringLookupTestCase struct {
	testName    string
	fileName    string
	keyToLookup string
	//TODO: replace this expected with the argument return type
	expectedReturn string
	expectedErr    bool
	expectedErrMsg string
}

func (testCase *jsonStringLookupTestCase) Run(t *testing.T) {
	t.Logf("running test: %v", testCase.testName)
	t.Logf("reading file: %v", testCase.fileName)

	fileContent, err := os.ReadFile(testCase.fileName)
	if err != nil {
		t.Logf("error while reading file of name: %v", testCase.fileName)
		t.Logf("error reported: %v", err)
		t.Fatalf("error in test %v while reading file: %v", testCase.testName, err)
	}

	//Function under test. Please change accordingly.
	got, err := String(&fileContent, testCase.keyToLookup)
	//check test ok

	assertOk := assert.Equal(t, testCase.expectedReturn, got, "asserting got: %v and expected: %v", got, testCase.expectedReturn)

	if !assertOk {
		t.Logf("got vs expected is different, assert failed")
		t.Logf("function reported error? : err : %v", err)
		t.FailNow()
		return
	}
	t.Logf("got vs expected ok")
	//check if expected error and got error are the same
	if testCase.expectedErr {
		assertErrOk := assert.EqualError(t, err, testCase.expectedErrMsg, "asserting error got: %v and expected: %v", err, testCase.expectedErrMsg)
		if !assertErrOk {
			t.Logf("found an diff in expected error, assert failed: gotErr: %v, expectedErr: %v", err, testCase.expectedErrMsg)
			t.FailNow()
			return
		}
	} else {
		//managed like this as error can be nil, but errMsg no.
		if err != nil {
			t.Logf("found an unexpected error, gotErr: %v", err)
			t.FailNow()
			return
		}
	}
	t.Logf("test: %v: OK ", testCase.testName)
	//t.Logf("unhandled test case ??? testCase data: %+v VS test results: got :%v, err: %v", testCase, got, err)
	//t.FailNow()
}
