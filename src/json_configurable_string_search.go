package lookup

import (
	"encoding/json"
	"fmt"
	"strings"
)

/*
StringLookupConfigs is used to set all error messages generating functions, with standarized useful arguments for debugging.
Useful in case you want to use a wrapper or analyze error messages in another instance of the application.
All  functions must be set, to avoid nil pointers.
*/
type StringLookupConfigs struct {
	DefaultReturnString      string
	EmptyKeyErrorMsg         func(keysSent []string) error
	UnmarshallingErrorMsg    func(keysSent []string, unmErr error) error
	NoValueFoundErrorMsg     func(keyToLookup string) error
	NotStringableErrorMsg    func(keyToLookup string, valueFound interface{}) error
	NextLevelUnmarshErrorMsg func(keyToLookup string, valueFoundNotUnmarshalled interface{}) error
}

/*StringSearchMissingFunctionsErrorMsg is the message shown when you forgot to set any error handling function */
const StringSearchMissingFunctionsErrorMsg string = "missing functions for lookup to work properly: %v"

/*
StringWithConfig function is used to analyze a json with specified error messages.
Its made intentionally to be  wrapped around in case you later want to recover and unwrap those errors.
*/
func StringWithConfig(contents *[]byte, path string, configs *StringLookupConfigs) (string, error) {

	err := validateStringConfig(configs)
	if err != nil {
		return configs.DefaultReturnString, err
	}
	keys := strings.Split(path, ".")
	if len(keys) == 0 {
		return configs.DefaultReturnString, configs.EmptyKeyErrorMsg(keys)
	}
	tempStorageMap := make(map[string]interface{})
	err = json.Unmarshal(*contents, &tempStorageMap)
	if err != nil {
		return configs.DefaultReturnString, configs.UnmarshallingErrorMsg(keys, err)
	}
	//setting up key to lookup for
	keyToLookup := keys[0]
	remainingHierarchyCalls := len(keys) - 1
	//found value?
	valueFound, valuePresent := tempStorageMap[keyToLookup]
	//case: value not present... so dead end
	if !valuePresent {
		return configs.DefaultReturnString, configs.NoValueFoundErrorMsg(keyToLookup)
	}
	//parser is apart and a nil pointer exception
	stringValue, stringable := parseToString(valueFound, *configs)
	//case: dont attempt to parse again, it's the final call and all methods have been tried
	if !stringable && remainingHierarchyCalls <= 0 {
		return configs.DefaultReturnString, configs.NotStringableErrorMsg(keyToLookup, valueFound)
	}
	//case: value present, but not stringable. it can be a json object, though
	if !stringable && remainingHierarchyCalls > 0 {
		newLevel, invalidNextLevelError := json.Marshal(valueFound)
		if invalidNextLevelError != nil {
			return configs.DefaultReturnString, configs.NextLevelUnmarshErrorMsg(keyToLookup, valueFound)
		}
		restOfPath := strings.Join(keys[1:], ".")
		//fmt.Printf("\n keys: %v,restOfP: %v", keys[1:], restOfPath)
		stringValue, err := StringWithConfig(&newLevel, restOfPath, configs)
		//fmt.Printf("\n recursive returned: %v,err: %v", stringValue, err)
		return stringValue, err
	}

	return stringValue, nil

}

func parseToString(valueFound interface{}, configs StringLookupConfigs) (stringValue string, stringable bool) {
	defer func() {
		parseError := recover()
		if parseError != nil {
			//overrides return value
			stringValue = configs.DefaultReturnString
			stringable = false
		}
	}()
	parsedAsString := fmt.Sprint(valueFound)
	fmt.Printf("\n ------ @! ACA, %v", parsedAsString)

	if parsedAsString == "{}" || parsedAsString == "{ }" {
		return configs.DefaultReturnString, stringable
	}
	fmt.Printf("\n @! ACA, %v", valueFound)
	stringValue, stringable = (valueFound).(string)
	fmt.Printf("\n FINALLY %v, %v", stringValue, stringable)
	return stringValue, stringable
}

//error messages that indicate the use case of the function that hasn't been set.
const (
	NoKeySentErrorFunctionDesc             string = "error function for empty or invalid key passed as arg"
	UnmarshallingErrorFunctionDesc         string = "error function for unmarshalling error"
	ValueNotFoundErrorFunctionDesc         string = "error function for value not found"
	UnableToParseToStringErrorFunctionDesc string = "error function for a non stringable element, due to type"
	NextLevelUnmarshallErrorFunctionDesc   string = "error function for invalid json in the next hierarchycal call"
)

//
func validateStringConfig(config *StringLookupConfigs) error {
	var missingFunctions []string
	if config.EmptyKeyErrorMsg == nil {
		missingFunctions = append(missingFunctions, NoKeySentErrorFunctionDesc)
	}
	if config.UnmarshallingErrorMsg == nil {
		missingFunctions = append(missingFunctions, UnmarshallingErrorFunctionDesc)
	}
	if config.NoValueFoundErrorMsg == nil {
		missingFunctions = append(missingFunctions, ValueNotFoundErrorFunctionDesc)
	}
	if config.NotStringableErrorMsg == nil {
		missingFunctions = append(missingFunctions, UnableToParseToStringErrorFunctionDesc)
	}
	if config.NextLevelUnmarshErrorMsg == nil {
		missingFunctions = append(missingFunctions, NextLevelUnmarshallErrorFunctionDesc)
	}
	if len(missingFunctions) != 0 {
		return fmt.Errorf(StringSearchMissingFunctionsErrorMsg, strings.Join(missingFunctions, ","))
	}
	return nil

}
